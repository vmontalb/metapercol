import os
from os.path import join
import sys

import graph_tool.all as gt
import numpy as np
import pandas as pd

BMS = os.environ["BMS"]
# Check BMS is defined in env

sys.path.append(BMS)
sys.path.append(join(BMS,'Prior'))

from mcmc import *
from parallel import *
from fit_prior import read_prior_par

import pdb

def giant_component(degree: np.array, occup: np.array, jdist: int):
    """
    This function will compute the (current) giant component of the network,
    taking into account the metadata/feautres of the node.
    Its based on the node degree, the occupancy (i.e already deleted or not),
    and the join degree-feautre metadata.

    Inputs:
        degree  :   Node-degree array (unweigthed)
                     (size N nodes)

        occup   :   The occupancy of each one of the nodes.
                     Binary array (1 or 0s) (size N nodes)

        jdist   :   Degree-metadata joint distribution.
                     Output from Bayesian Machine Scientist (Guimera etal 2021)

    Output:
        cgiant  :   Giant Component of the network
    """

def calc_jdist(degree: np.array, features: np.array):
    """
    Calculate the degree-feautre joint distribution.
    It first compute the two-dimensional histogram of both features, and then
    use Bayesian Machine Scientits (BMS - Guimera etal) to compute the P(k,F)
    joint distribution.
    Further work will integrate N-dimensional features

    Inputs:
        degree   :   Node-degree array (unweigthed)
                     (size: N nodes)

        features :   Continuous/Discreate values of metadata/feautes
                     (siz:e N nodes)

    Output:
        jdist    :   Output
    """
    # BMS for each k-degree
    # Defaults
    nstep = 10
    Ts = [1] + [1.04**k for k in range(1, 20)]
    prior_par = read_prior_par(join(BMS,'./Prior/final_prior_param_sq.named_equations.nv13.np13.2016-09-01 17:05:57.196882.dat'))
    modelz = list()
    klist = list()

    # Loop k-degree to get the probability distribution function
    for idx in range(0,np.unique(degree).size):
        # Select feature-frequency for each k-degree (histogram)
        currk = features[degree == idx]
        if currk.size == 0:
            continue
        nbins = np.ceil(np.unique(currk).size * 0.75).astype(int)
        hist,bin_edges = np.histogram(currk, bins = nbins, density = True) #Freq normalized
        # Filter to non-zero values (prevent NaNs in BMS)




        pdb.set_trace()
        # Filter out those k with low values
        if bin_edges.size < 20:
            continue
        klist.append(idx)

        # Prepare data for BMS (must be DataFrame and Serie)
        x = pd.DataFrame(data = {'values': bin_edges })
        XLABS = ['values']
        frequency = pd.Series(hist)

        # Init PMS
        pms = Parallel(
            Ts,
            variables=XLABS,
            parameters=['a%d' % i for i in range(len(XLABS))],
            x=x, y=frequency,
            prior_par=prior_par,
        )

        # Starts BMC MCMC
        pdb.set_trace()
        description_lengths, mdl, mdl_model = [], np.inf, None
        for i in range(nstep):
            # MCMC update
            pms.mcmc_step() # MCMC step within each T
            pms.tree_swap() # Attempt to swap two randomly selected consecutive temperature
            # Add the description length to the trace
            description_lengths.append(pms.t1.E)
            # Check if this is the MDL expression so far
            if pms.t1.E < mdl:
                mdl, mdl_model = pms.t1.E, deepcopy(pms.t1)

        # Save best model fit for current k-degree
        pdb.set_trace()
        modelz.append(mdl_model)

    # Compute h(k)function
    maxfreq = np.max(hist, dim = 1)
    nstep = 20000
    # Init PMS
    pms = Parallel(
        Ts,
        variables=XLABS,
        parameters=['a%d' % i for i in range(13)],
        x=x, y=y,
        prior_par=prior_par,
    )
    # Starts BMC MCMC
    description_lengths, mdl, mdl_model = [], np.inf, None
    for i in range(nstep):
        # MCMC update
        pms.mcmc_step() # MCMC step within each T
        pms.tree_swap() # Attempt to swap two randomly selected consecutive temperature
        # Add the description length to the trace
        description_lengths.append(pms.t1.E)
        # Check if this is the MDL expression so far
        if pms.t1.E < mdl:
            mdl, mdl_model = pms.t1.E, deepcopy(pms.t1)
    # Save h(k)
    hmodelz = mdl_model

    # save joint-distribution
    return modelz, hmodelz
