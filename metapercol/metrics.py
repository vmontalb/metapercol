from __future__ import absolute_import, division, print_function

import os
from os import fstat
from sys import stdout
from math import ceil

import graph_tool.all as gt
import numpy as np

def degree_attack(g: gt.Graph, mod: str='total'):
    """
    Delete nodes in descending order based on total degree

    Inputs:
       g     : gt.Graph() object. Input graph

    Return:
       order :  np.array. Order (inverse) of nodes to percolate
       S     :  np.array. Size of bigger cluster after percolations

    Notes:
        The S parameter is based on percolation function as implemented in
    graph tool.
https://graph-tool.skewed.de/static/doc/_modules/graph_tool/topology.html#vertex_percolation

    """
    return


def high_betweenness_attack(g: gt.Graph):
    """
    Delete nodes in descending order based on betweeness
    A formal descritpion of between can be found here:
       < https://en.wikipedia.org/wiki/Betweenness_centrality >
    Inputs:
       g     : gt.Graph() object

    Return:
       order :  np.array. Order (inverse) of nodes to percolate
       S     :  np.array. Size of bigger cluster after percolations

    Notes:
        The S parameter is based on percolation function as implemented in
    graph tool.
https://graph-tool.skewed.de/static/doc/_modules/graph_tool/topology.html#vertex_percolation

    """
    return

def module_connectors_attack(g: gt.Graph):
    """
    Detect nodes that are connecting different modules and attack them
    The attack is based on their betweeness centrality measure.

    Inputs:
       g     : gt.Graph() object. Input graph

    Return:
       order :  np.array. Order (inverse) of nodes to percolate
       S     :  np.array. Size of bigger cluster after percolations

    Refs:
       Cunha etal 2015 Plos One

    Notes:
        - The community detection method applied is based on SBM (by Peixoto et al)
        It uses the function gt.minimize_blockmodel_dl(g)
        Since its an stochastic model, slight differences at every run might happen

        - The S parameter is based on percolation function as implemented in
    graph tool.
https://graph-tool.skewed.de/static/doc/_modules/graph_tool/topology.html#vertex_percolation
    """
    return

def random_percolation(g: gt.Graph, n: int = 500):
    """
    Percolation attacking random nodes at a time
    It will create a null-model based on n-iterations
    The median value will be stored

    Inputs:
       g     : gt.Graph() object. Input graph
       n     : int. Number iterations

    Return:
       order :  np.array. Order (inverse) of nodes to percolate
       S     :  np.array. Size of bigger cluster after percolations

    """
    return
